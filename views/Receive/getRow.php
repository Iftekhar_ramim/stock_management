<?php
session_start();
include_once('../../vendor/autoload.php');
use App\Bitm\Medicine\Medicine;
$cobj=new Medicine();
$allc=$cobj->dropdown();


if(isset($_POST['data'])) {
    $row_id = $_POST['data'];

    $data = $row_id + 1;

    // return $data;
    $vcount=count($allc);

    echo "
    <tr class='drow-$data' data=$data>
        <td class='center'>
            <label class='pos-rel'>
                <input type='checkbox' class='ace' data=$data/>
                <span class='lbl'></span>
            </label>
        </td>
    
        <td>
            <select class='form-control cat_id1' id='cat_id1-$data' data-placeholder='Choose a State...' name='cat_id[]' data=$data >
                <option value=''>Select Catagory</option>";
                if ($vcount > 0) {
                    foreach ($allc as $allcat) {
                        echo "<option value=$allcat[id]> $allcat[cat_name] </option>";
                    }
                } else {
                    echo "<option value=''>Medicine not available</option>";
                }
           echo "</select>
        </td>
        <td>
            <select class='form-control medi_id1' id='medi_id1-$data' data-placeholder='Choose a State...' name='medi_id[]' data=$data >
                <option value=''>Select Medicine</option>
            </select>
        </td>
        <td class='hidden-480'>  
            <select class='form-control mucd1' id='mucd1-$data' data-placeholder='Choose a State...' name='unitid[]' data=$data >
                <option value=''>Select Unit</option>
            </select>
        </td>
        <td> <input class='form-control'  type='text'  name='qty[]' data=$data/></td>
    
        <td class='hidden-480'>
            <input class='form-control'  type='text'  name='amount[]' data=$data/>
        </td>
    
        <td data=$data>
            <div class='hidden-sm hidden-xs btn-group' data=$data>
                <button type='button' id='addrow-$data' data=$data class='btn btn-xs btn-success addrow'>
                    <i class='ace-icon fa fa-check bigger-120'></i>
                </button>
                <button type='button' id='delrow-$data' data=$data class='btn btn-xs btn-danger delrow'>
                    <i class='ace-icon fa fa-minus bigger-120'></i>
                </button>
            </div>
        </td>
    </tr>
    <script>
        $('.delrow').hide();
    </script>
    
    <script type='text/javascript'>
        
            $('.addrow').click(function(){
                data = $(this).attr('data');
                $.ajax({
                    type:'POST',
                    url:'getRow.php',
                    data: {data: data},
                    success:function(response){
                        $('.tbd').append(response);
                        $('#addrow-'+data).addClass('hide');
                        $('#delrow-'+data).addClass('show');
                    }
                })
            });

    </script>
    <script type='text/javascript'>
            $('.cat_id1').on('change',function(){
                var catID = $(this).val();
                var data = $(this).attr('data');
                if(catID){
                    $.ajax({
                        type:'POST',
                        url:'ajaxcat.php',
                        data:'cat_id='+catID,
                        success:function(html){
                            $('#medi_id1-'+data).html(html);
                            $('#mucd1-'+data).html('<option value=>Select Medicine first</option>');
                        }
                    });
                }else{
                    $('#medi_id1'+data).html('<option value=>Select catagory first</option>');
                    $('#mucd1'+data).html('<option value=>Select medicine first</option>');
                }
            });
            $('.medi_id1').on('change',function(){
                var mediID = $(this).val();
                var data = $(this).attr('data');
                if(mediID){
                    $.ajax({
                        type:'POST',
                        url:'ajaxcat.php',
                        data:'medi_id='+mediID,
                        success:function(html){
                            $('#mucd1-'+data).html(html);
                        }
                    });
                }else{
                    $('#mucd1'+data).html('<option value=>Select medicine first</option>');
                }
            });
    </script>
    
    ";
}