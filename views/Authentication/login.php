<?php
include_once('../../vendor/autoload.php');
session_start();
use App\Bitm\Users\Users;
use App\Bitm\Users\Auth;
use App\Bitm\Message\Message;
use App\Bitm\Utility\Utility;

$auth= new Auth();
$status= $auth->prepare($_POST)->is_registered();

if($status){
    $_SESSION['ses_user']=$_POST['user_name'];
    return Utility::redirect('../../dashboard.php');

}else{
    Message::message("<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Please check your email or password
</div>");
    Utility::redirect('../../index.php');
}

