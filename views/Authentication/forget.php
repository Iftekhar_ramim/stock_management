<?php
include_once('../../vendor/autoload.php');
session_start();
use App\Bitm\Users\Users;
use App\Bitm\Users\Auth;
use App\Bitm\Message\Message;
use App\Bitm\Utility\Utility;

$dt=date("h:i:sa");
//echo $dt;
//die();

$auth= new Auth();
$exist= $auth->prepare($_POST)->is_exist();

if($exist){
    $vusr= $auth->prepare($_POST)->viewuser();
    if ($vusr) {
        $_POST['user_name']=$vusr['user_name'];
        $_POST['passkey']=md5($_POST['user_name'].$_POST['email'].$dt);
//        echo $_POST['passkey'];
//        die();
        $chk=$auth->prepare($_POST)->is_checked();
//        var_dump($chk);
//        die();
        if ($chk){
            $auth->update_reset_pass();
            $sinfo=$auth->sendinfo();
//            var_dump($sinfo);
//            echo $sinfo['user_name'];
//            echo "</br>";
//            echo $_POST['email'];
//            echo "</br>";
//            echo $sinfo['mkey'];
//            die();
            //$response = file_get_contents("http://localhost/pharmacy_stock/views/Authentication/sendkey.php?user_name=".$sinfo['user_name']."&email=".$_POST['email']."&mkey=".$sinfo['mkey']);
            $response = file_get_contents("http://localhost/pharma/views/Authentication/sendkey.php?user_name=". $sinfo['user_name']. "&email=" . $_POST['email'] . "&mkey=". $sinfo['mkey']);
            //echo $response;
            if ($response=='Message sent!'){
                Message::message("<div class=\"alert alert-success\">
                                    <strong>Success!</strong> Password Reset.Please check your mail.
                                    </div>");
                header('Location:../../index.php');
            } else {
                Message::message("<div class=\"alert alert-danger\">
                                    <strong>Error!</strong> Mail Not Sent.Cantact with administrator.
                                    </div>");
                Utility::redirect('../../index.php');
            }
        } else {
            $npass=$auth->new_pass_req();
//            var_dump($npass);
//            die();
            if ($npass){
                $sinfo=$auth->sendinfo();
                $response = file_get_contents("http://localhost/pharma/views/Authentication/sendkey.php?user_name=". $sinfo['user_name']. "&email=" . $_POST['email'] . "&mkey=". $sinfo['mkey']);
                //echo $response;
                if ($response=='Message sent!'){
                    Message::message("<div class=\"alert alert-success\">
                                    <strong>Success!</strong> Password Reset.Please check your mail.
                                    </div>");
                    header('Location:../../index.php');
                } else {
                    Message::message("<div class=\"alert alert-danger\">
                                    <strong>Error!</strong> Mail Not Sent.Cantact with administrator.
                                    </div>");
                    Utility::redirect('../../index.php');
                }
            } else {
                Message::message("<div class=\"alert alert-danger\">
                                    <strong>Error!</strong> Cannot reset password.Cantact with administrator.
                                    </div>");
                Utility::redirect('../../index.php');
            }
                
        }
    }
}else{
    Message::message("<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Please check your email or register
</div>");
    Utility::redirect('../../index.php');
}