<?php
include_once('../../vendor/autoload.php');
session_start();
use App\Bitm\Users\Users;
use App\Bitm\Users\Auth;
use App\Bitm\Message\Message;
use App\Bitm\Utility\Utility;

$auth= new Auth();
$status=$auth->logout();
if($status){
    return Utility::redirect('../../index.php');
}