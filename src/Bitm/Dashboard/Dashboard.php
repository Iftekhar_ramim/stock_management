<?php
/**
 * Created by PhpStorm.
 * User: freak
 * Date: 31-Jul-16
 * Time: 3:35 AM
 */

namespace App\Bitm\Dashboard;
use App\Bitm\Message\Message;
use App\Bitm\Utility\Utility;

class Dashboard{

    public function __construct()
    {
        $this->conn= mysqli_connect("localhost","root","","pstock") or die("Database connection failed");
    }

    public function count_supp(){
        $_csupp= array();
        $query="SELECT COUNT(`id`) FROM `supplier` WHERE `status`='on'";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_csupp[]=$row;
        }
        return $_csupp;
    }

    public function count_cust(){
        $_cust= array();
        $query="SELECT COUNT(`id`) FROM `customer` WHERE `status`='on'";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_cust[]=$row;
        }
        return $_cust;
    }

    public function count_cat(){
        $_cat= array();
        $query="SELECT COUNT(`id`) FROM `catagory` WHERE `status`='on'";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_cat[]=$row;
        }
        return $_cat;
    }

    public function count_med(){
        $_med= array();
        $query="SELECT COUNT(`id`) FROM `medicine` WHERE `status`='on'";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_med[]=$row;
        }
        return $_med;
    }

    public function count_recv(){
        $_recv= array();
        $query="SELECT COUNT(`id`) FROM `rcv_master`";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_recv[]=$row;
        }
        return $_recv;
    }

    public function count_issu(){
        $_issu= array();
        $query="SELECT COUNT(`id`) FROM `issue_master`";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_issu[]=$row;
        }
        return $_issu;
    }

    public function sum_rcvamt(){
        $_rcvamt= array();
        $query="SELECT SUM(`amount`) AS amount FROM `rcv_detail`";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_rcvamt[]=$row;
        }
        return $_rcvamt;
    }
    
    public function sum_issamt(){
        $_issamt= array();
        $query="SELECT SUM(`amount`) AS amount FROM `issue_detail`";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_issamt[]=$row;
        }
        return $_issamt;
    }

    public function userinfo(){
        $_usrinfo= array();
        $query="SELECT * FROM `users`";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_usrinfo[]=$row;
        }
        return $_usrinfo;
    }
    
}