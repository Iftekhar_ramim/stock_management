<?php
namespace App\Bitm\Medicine;
use App\Bitm\Message\Message;
use App\Bitm\Utility\Utility;

Class Medicine{
    public $cat_id;
    public $cat_name;
    public $id;
    public $conn;
    public $medi_id;
    public $medicine;
    public $itemid;
    public $status;
    public $unitid;

    ////////prepare data
    public function prepare($data=Array()){

        if (array_key_exists("id",$data)){
            $this->id=$data['id'];
        }

        if (array_key_exists("cat_id",$data)){
            $this->cat_id=$data['cat_id'];
        }
        if (array_key_exists("medicine",$data)){
            $this->medicine=$data['medicine'];
        }
        if (array_key_exists("medi_id",$data)){
            $this->medi_id=$data['medi_id'];
        }
        if (array_key_exists("status",$data)){
            $this->status=$data['status'];
        }

        if (array_key_exists("itemid",$data)){
            $this->itemid=$data['itemid'];
        }

        if (array_key_exists("unitid",$data)){
            $this->unitid=$data['unitid'];
        }
        return $this;
    }

    //////prepare connection
    public function __construct()
    {
        $this->conn= mysqli_connect("localhost","root","","pstock") or die("Database connection failed");
    }

    ////////prepare to store or insert data
    public function store()
    {
        $query = "INSERT INTO `pstock`.`medicine` (`cat_id`,`medicine`,`unitid`,`status`)
                        VALUES ('" . $this->cat_id . "','" . $this->medicine . "','" . $this->unitid . "','" . $this->status . "')";


        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class=\"alert alert-success\">
                                    <strong>Success!</strong> Data has been stored successfully.
                                    </div>");
            header('Location:create.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
                                    <strong>Error!</strong> Data has not been stored successfully.
                                    </div>");
            Utility::redirect('create.php');
        }
    }

    public function index(){
        $_all = array();
        $query= "SELECT m.id,m.cat_id,c.cat_name,m.medicine,m.status,u.MUNM FROM medicine m,catagory c,measer_info u WHERE m.cat_id=c.id AND m.unitid=u.MUCD AND m.deleted_at IS NULL ORDER BY m.id";
        $result= mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_assoc($result)){
            $_all[]=$row;
        }

        return $_all;
    }

    public function ajaxcat(){
        $_all = array();
        $query="SELECT * FROM `medicine` WHERE `cat_id`=".$this->cat_id;
        $result= mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_assoc($result)){
            $_all[]=$row;
        }

        return $_all;
    }

    public function ajaxunit(){
        $_all = array();
        $query="SELECT * FROM measer_info WHERE MUCD='".$this->unitid."'";
//        print_r($query);
//        die();
        $result= mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_assoc($result)){
            $_all[]=$row;
        }

        return $_all;
    }


    public function trashlist(){
        $_all = array();
        $query= "SELECT m.id,m.cat_id,c.cat_name,m.medicine,m.status,u.MUNM FROM medicine m,catagory c,measer_info u WHERE m.cat_id=c.id AND m.unitid=u.MUCD AND m.deleted_at IS NOT NULL ORDER BY m.id";
        $result= mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_assoc($result)){
            $_all[]=$row;
        }

        return $_all;
    }



    public function view(){
        $query="SELECT * FROM `medicine` WHERE `id`=".$this->id;
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;

    }
    public function uview(){
        $query="SELECT * FROM `medicine` WHERE `id`=".$this->medi_id;
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;

    }


    public function update(){
        $query="UPDATE `medicine`
            SET `cat_id` = '".$this->cat_id."',`medicine` = '".$this->medicine."',`status` = '".$this->status."',`unitid` = '" . $this->unitid . "'
             WHERE `medicine`.`id` =".$this->id;
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
            <strong>Updated!</strong> Data has been Updated successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-warning\">
            <strong>Error!</strong> Data has not been updated  successfully.
            </div>");
            Utility::redirect('index.php');

        }

    }

    public function delete(){
        $query="DELETE FROM `pstock`.`medicine` WHERE `medicine`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been deleted successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been deleted successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }

    public function trash(){
        $query="UPDATE `pstock`.`medicine` SET `deleted_at` = '".time()."' WHERE `medicine`.`id` = ".$this->id;
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Trashed successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Trashed successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }

    public function restore(){
        $query="UPDATE `pstock`.`medicine` SET `deleted_at` = NULL WHERE `medicine`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Restore successfully.
            </div>");
            header('Location:trashed_list.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Restore successfully.
            </div>");
            Utility::redirect('trashed_list.php');
        }
    }

    public function trashmultiple(){
        $query="UPDATE `pstock`.`medicine` SET `deleted_at` = '".time()."' WHERE `medicine`.`id` IN(".$this->itemid.")";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Trashed successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Trashed successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }

    public function restoremultiple(){
        $query="UPDATE `pstock`.`medicine` SET `deleted_at` = NULL WHERE `medicine`.`id` IN(".$this->itemid.")";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Restore successfully.
            </div>");
            header('Location:trashed_list.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Restore successfully.
            </div>");
            Utility::redirect('trashed_list.php');
        }
    }

    public function deletemultiple(){
        $query="DELETE FROM `pstock`.`medicine` WHERE `medicine`.`id` IN(".$this->itemid.")";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Deleted successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Deleted successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }

    public function dropdown(){
        $all_dropdown=array();
        $dropdown = "SELECT * FROM catagory WHERE status='on'";
        $dropresult=mysqli_query($this->conn,$dropdown);
        while($row= mysqli_fetch_assoc($dropresult)){
            $all_dropdown[]=$row;
        }
        return $all_dropdown;
    }

    public function unit(){
        $all_dropdown=array();
        $dropdown = "SELECT * FROM measer_info";
        $dropresult=mysqli_query($this->conn,$dropdown);
        while($row= mysqli_fetch_assoc($dropresult)){
            $all_dropdown[]=$row;
        }
        return $all_dropdown;
    }
}//////////End Bracket