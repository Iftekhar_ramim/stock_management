<?php
namespace App\Bitm\Catagory;
use App\Bitm\Message\Message;
use App\Bitm\Utility\Utility;

Class Catagory{
    public $id="";
    public $cat_name="";
    public $status="";
    public $conn;

    ////////prepare data
    public function prepare($data=Array()){
        if (array_key_exists("cat_name",$data)){
            $this->cat_name=$data['cat_name'];
        }
        if (array_key_exists("id",$data)){
            $this->id=$data['id'];
        }
        
        if (array_key_exists("status",$data)){
            $this->status=$data['status'];
        }
        if (array_key_exists("itemid",$data)){
            $this->itemid=$data['itemid'];
        }
        return $this;
    }

    //////prepare connection
    public function __construct()
    {
        $this->conn= mysqli_connect("localhost","root","","pstock") or die("Database connection failed");
    }

    ////////prepare to store or insert data
    public function store()
    {
        $query = "INSERT INTO `pstock`.`catagory` (`cat_name`,`status`)
                        VALUES ('" . $this->cat_name . "','" . $this->status . "')";


        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class=\"alert alert-success\">
                                    <strong>Success!</strong> Data has been stored successfully.
                                    </div>");
            header('Location:create.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
                                    <strong>Error!</strong> Data has not been stored successfully.
                                    </div>");
            Utility::redirect('create.php');
        }
    }

    public function index(){
        $_allcatagory= array();
        $query="SELECT * FROM `catagory` WHERE `deleted_at` IS NULL";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_allcatagory[]=$row;
        }
        return $_allcatagory;
    }

    public function trashlist(){
        $_alltrashed= array();
        $query="SELECT * FROM `catagory` WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_alltrashed[]=$row;
        }
        return $_alltrashed;
    }



    public function view(){
        $query="SELECT * FROM `catagory` WHERE `id`=".$this->id;
        // echo $query;
        // die();
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;

    }

//update

    public function update(){
        $query="UPDATE `catagory`
            SET `cat_name` = '".$this->cat_name."',`status` = '".$this->status."'
             WHERE `catagory`.`id` =".$this->id;
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
            <strong>Updated!</strong> Data has been Updated successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-warning\">
            <strong>Error!</strong> Data has not been updated  successfully.
            </div>");
            Utility::redirect('index.php');

        }

    }

    public function delete(){
        $query="DELETE FROM `pstock`.`catagory` WHERE `catagory`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been deleted successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been deleted successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }
//trash

    public function trash(){
        $query="UPDATE `pstock`.`catagory` SET `deleted_at` = '".time()."' WHERE `catagory`.`id` = ".$this->id;
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Trashed successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Trasded successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }

    public function restore(){
        $query="UPDATE `pstock`.`catagory` SET `deleted_at` = NULL WHERE `catagory`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Restore successfully.
            </div>");
            header('Location:trashed_list.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Restore successfully.
            </div>");
            Utility::redirect('trashed_list.php');
        }
    }

    public function trashmultiple(){
        $query="UPDATE `pstock`.`catagory` SET `deleted_at` = '".time()."' WHERE `catagory`.`id` IN(".$this->itemid.")";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Trashed successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Trashed successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }

    public function restoremultiple(){
        $query="UPDATE `pstock`.`catagory` SET `deleted_at` = NULL WHERE `catagory`.`id` IN(".$this->itemid.")";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Trashed successfully.
            </div>");
            header('Location:trashed_list.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Trashed successfully.
            </div>");
            Utility::redirect('trashed_list.php');
        }
    }

    public function deletemultiple(){
        $query="DELETE FROM `pstock`.`catagory` WHERE `catagory`.`id` IN(".$this->itemid.")";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Deleted successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Deleted successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }
}//////////End Bracket