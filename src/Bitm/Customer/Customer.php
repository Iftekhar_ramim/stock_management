<?php
namespace App\Bitm\Customer;
use App\Bitm\Message\Message;
use App\Bitm\Utility\Utility;

Class Customer{
    public $id="";
    public $name="";
    public $contact_no="";
    public $address="";
    public $status="";
    public $deleted_at="";
    public $conn;

    ////////prepare data
    public function prepare($data=Array()){
        if (array_key_exists("name",$data)){
            $this->name=$data['name'];
        }
        if (array_key_exists("contact_no",$data)){
            $this->contact_no=$data['contact_no'];
        }
        if (array_key_exists("address",$data)){
            $this->address=$data['address'];
        }
        if (array_key_exists("status",$data)){
            $this->status=$data['status'];
        }
        if (array_key_exists("id",$data)){
            $this->id=$data['id'];
        }
        if (array_key_exists("itemid",$data)){
            $this->itemid=$data['itemid'];
        }

        return $this;
    }

    //////prepare connection
    public function __construct()
    {
        $this->conn= mysqli_connect("localhost","root","","pstock") or die("Database connection failed");
    }

    ////////prepare to store or insert data
    public function store()
    {

        $query="INSERT INTO `pstock`.`customer` (`name`, `contact_no`, `address`, `status`) VALUES ('".$this->name."', '".$this->contact_no."', '".$this->address."', '".$this->status."')";

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("<div class=\"alert alert-success\">
                                    <strong>Success!</strong> Data has been stored successfully.
                                    </div>");
            header('Location:create.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
                                    <strong>Error!</strong> Data has not been stored successfully.
                                    </div>");
            Utility::redirect('create.php');
        }
    }

    public function index(){
        $_allcustomer= array();
        $query="SELECT * FROM `customer`WHERE `deleted_at` IS NULL";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_allcustomer[]=$row;
        }
        return $_allcustomer;
    }

    public function trashlist(){
        $_allcustomerl= array();
        $query="SELECT * FROM `customer` WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_allcustomerl[]=$row;
        }
        return $_allcustomerl;
    }


    public function view(){
        $query="SELECT * FROM `customer` WHERE `id`=".$this->id;
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;

    }


    public function update(){

        $query="UPDATE `customer` SET `name` = '".$this->name." ', `contact_no` = '".$this->contact_no."', `address` = '".$this->address."', `status` = '".$this->status."' WHERE `customer`.`id` =".$this->id;

//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
            <strong>Updated!</strong> Data has been Updated successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-warning\">
            <strong>Error!</strong> Data has not been updated  successfully.
            </div>");
            Utility::redirect('index.php');

        }

    }

    public function delete(){
        $query="DELETE FROM `pstock`.`customer` WHERE `customer`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been deleted successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been deleted successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }

    public function trash(){
        $query="UPDATE `pstock`.`customer` SET `deleted_at` = '".time()."' WHERE `customer`.`id` = ".$this->id;
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Trashed successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Trasded successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }

    public function restore(){
        $query="UPDATE `pstock`.`customer` SET `deleted_at` = NULL WHERE `customer`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Restore successfully.
            </div>");
            header('Location:trashed_list.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Restore successfully.
            </div>");
            Utility::redirect('trashed_list.php');
        }
    }

    public function trashmultiple(){
        $query="UPDATE `pstock`.`customer` SET `deleted_at` = '".time()."' WHERE `customer`.`id` IN(".$this->itemid.")";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Trashed successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Trashed successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }

    public function restoremultiple(){
        $query="UPDATE `pstock`.`customer` SET `deleted_at` = NULL WHERE `customer`.`id` IN(".$this->itemid.")";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Trashed successfully.
            </div>");
            header('Location:trashed_list.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Trashed successfully.
            </div>");
            Utility::redirect('trashed_list.php');
        }
    }

    public function deletemultiple(){
        $query="DELETE FROM `atomicprojectb21`.`email` WHERE `email`.`id` IN(".$this->itemid.")";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Deleted successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Deleted successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }
}//////////End Bracket