<?php
namespace App\Bitm\Reports;
use App\Bitm\Message\Message;
use App\Bitm\Utility\Utility;

Class Reports{
    public $id="";
    public $date="";
    public $medi_id="";
    public $customer_id="";
    public $supp_id="";
    public $conn;

    ////////prepare data
    public function prepare($data=Array()){
        if (array_key_exists("date",$data)){
            $this->date=$data['date'];
        }
        if (array_key_exists("medi_id",$data)){
            $this->medi_id=$data['medi_id'];
        }
        if (array_key_exists("supp_id",$data)){
            $this->supp_id=$data['supp_id'];
        }
        if (array_key_exists("customer_id",$data)){
            $this->customer_id=$data['customer_id'];
        }
        if (array_key_exists("id",$data)){
            $this->id=$data['id'];
        }
        return $this;
    }

    //////prepare connection
    public function __construct()
    {
        $this->conn= mysqli_connect("localhost","root","","pstock") or die("Database connection failed");
    }

    ////////prepare to store or insert data


    public function crstk(){
        $_allstk= array();
        $query="select med.medcine as medid,a.medicine as mednm,med.unit as mucd,b.munm as munm,sum(med.receive) as rcv,sum(med.issue) as iss,SUM(IfNull(med.receive, 0))-SUM(IfNull(med.issue, 0)) as stock from 
            (select medi_id as medcine,unitid as unit,sum(qty) as receive,0 issue from rcv_detail where master_date<='".$this->date."' group by medi_id 
            union
            select medi_id as medcine,unitid as unit,0 receive,sum(qty) as issue from issue_detail where master_date<='".$this->date."' group by medi_id) as med,
            medicine a,
            measer_info b
            where a.id=med.medcine
            and med.unit=b.MUCD
            group by medcine";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_allstk[]=$row;
        }
        return $_allstk;
    }

    
    public function view(){
        $query="SELECT * FROM `rcv_master` WHERE `id`=".$this->id;
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;

    }

    public function issview(){
        $query="SELECT * FROM `issue_master` WHERE `id`=".$this->id;
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;

    }

    public function suppview(){
        $query="SELECT * FROM `supplier` WHERE `id`=".$this->supp_id;
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;

    }

    public function custview(){
        $query="SELECT * FROM `customer` WHERE `id`=".$this->customer_id;
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;

    }

    public function rcvinvoicedetail(){
        $_allrcvd= array();
        $query="SELECT d.id,d.master_id,d.master_date,c.cat_name,m.medicine,u.MUNM,d.qty,d.amount FROM rcv_detail d,catagory c,medicine m,measer_info u WHERE c.id=d.cat_id
            AND d.medi_id=m.id
            and d.unitid=u.MUCD
            AND d.master_id=".$this->id;

//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_allrcvd[]=$row;
        }
        return $_allrcvd;
    }

    public function issinvoicedetail(){
        $_allrcvd= array();
        $query="SELECT d.id,d.master_id,d.master_date,c.cat_name,m.medicine,u.MUNM,d.qty,d.amount FROM issue_detail d,catagory c,medicine m,measer_info u WHERE c.id=d.cat_id
            AND d.medi_id=m.id
            and d.unitid=u.MUCD
            AND d.master_id=".$this->id;

//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_allrcvd[]=$row;
        }
        return $_allrcvd;
    }


    public function update(){
        $query="UPDATE `supplier`
            SET `supp_name` = '".$this->supp_name."',`address` = '".$this->address."',`status` = '".$this->status."'
             WHERE `supplier`.`id` =".$this->id;
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
            <strong>Updated!</strong> Data has been Updated successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-warning\">
            <strong>Error!</strong> Data has not been updated  successfully.
            </div>");
            Utility::redirect('index.php');

        }

    }

    public function delete(){
        $query="DELETE FROM `pstock`.`supplier` WHERE `supplier`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been deleted successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been deleted successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }

    public function trash(){
        $query="UPDATE `pstock`.`supplier` SET `deleted_at` = '".time()."' WHERE `supplier`.`id` = ".$this->id;
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Trashed successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Trasded successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }

    public function restore(){
        $query="UPDATE `pstock`.`supplier` SET `deleted_at` = NULL WHERE `supplier`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Restore successfully.
            </div>");
            header('Location:trashed_list.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Restore successfully.
            </div>");
            Utility::redirect('trashed_list.php');
        }
    }

    public function trashmultiple(){
        $query="UPDATE `pstock`.`supplier` SET `deleted_at` = '".time()."' WHERE `supplier`.`id` IN(".$this->itemid.")";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Trashed successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Trashed successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }

    public function restoremultiple(){
        $query="UPDATE `pstock`.`supplier` SET `deleted_at` = NULL WHERE `supplier`.`id` IN(".$this->itemid.")";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Trashed successfully.
            </div>");
            header('Location:trashed_list.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Trashed successfully.
            </div>");
            Utility::redirect('trashed_list.php');
        }
    }

    public function deletemultiple(){
        $query="DELETE FROM `pstock`.`supplier` WHERE `supplier`.`id` IN(".$this->itemid.")";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Deleted successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Deleted successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }
}//////////End Bracket