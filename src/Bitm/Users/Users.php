<?php
namespace App\Bitm\Users;
use App\Bitm\Message\Message;
use App\Bitm\Utility\Utility;

Class Users{
    public $id="";
    public $user_name="";
    public $password="";
    public $email="";
    public $role="";
    public $status="";
    public $conn;

    ////////prepare data
    public function prepare($data=Array()){
        if (array_key_exists("user_name",$data)){
            $this->user_name=$data['user_name'];
        }
        if (array_key_exists("id",$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('password',$data)){
            $this->password=md5($data['password']);
        }
        if (array_key_exists("email",$data)){
            $this->email=$data['email'];
        }
        if (array_key_exists("status",$data)){
            $this->status=$data['status'];
        }
        if (array_key_exists("role",$data)){
            $this->role=$data['role'];
        }
        if (array_key_exists("itemid",$data)){
            $this->itemid=$data['itemid'];
        }
        return $this;
    }

    //////prepare connection
    public function __construct()
    {
        $this->conn= mysqli_connect("localhost","root","","pstock") or die("Database connection failed");
    }

    ////////prepare to store or insert data
    public function store()
    {
        $query1="SELECT * FROM `users` WHERE `email`='".$this->email."' OR `user_name`='".$this->user_name."'";
        $result= mysqli_query($this->conn,$query1);
        $row= mysqli_num_rows($result);
        if($row>0){
            Message::message("<div class=\"alert alert-danger\">
                                    <strong>Error!</strong> User or Email already exist.
                                    </div>");
            Utility::redirect('create.php');
        }
        else {
            $query = "INSERT INTO `pstock`.`users` (`user_name`,`password`,`email`,`role`,`status`)
                        VALUES ('" . $this->user_name . "','" . $this->password . "','" . $this->email . "','" . $this->role . "','" . $this->status . "')";


            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("<div class=\"alert alert-success\">
                                    <strong>Success!</strong> Data has been stored successfully.
                                    </div>");
                header('Location:create.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
                                    <strong>Error!</strong> Data has not been stored successfully.
                                    </div>");
                Utility::redirect('create.php');
            }
        }
        //End Test
        
    }

    public function index(){
        $_allsupplier= array();
        $query="SELECT * FROM `users`";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_allsupplier[]=$row;
        }
        return $_allsupplier;
    }



    public function view(){
        $query="SELECT * FROM `users` WHERE `id`=".$this->id;
//         echo $query;
//         die();
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;

    }


    public function update(){
        $query="UPDATE `users`
            SET `user_name` = '".$this->user_name."',`password` = '".$this->address."',`email` = '".$this->email."',`role` = '".$this->role."',`status` = '".$this->status."'
             WHERE `users`.`id` =".$this->id;
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
            <strong>Updated!</strong> Data has been Updated successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-warning\">
            <strong>Error!</strong> Data has not been updated  successfully.
            </div>");
            Utility::redirect('index.php');

        }

    }

    public function delete(){
        $query="DELETE FROM `pstock`.`users` WHERE `users`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been deleted successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been deleted successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }


    public function restore(){
        $query="UPDATE `pstock`.`user` SET `deleted_at` = NULL WHERE `pstock`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Restore successfully.
            </div>");
            header('Location:trashed_list.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Restore successfully.
            </div>");
            Utility::redirect('trashed_list.php');
        }
    }



    public function deletemultiple(){
        $query="DELETE FROM `pstock`.`users` WHERE `users`.`id` IN(".$this->itemid.")";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Deleted successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Deleted successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }
}//////////End Bracket