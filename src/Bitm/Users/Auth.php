<?php

namespace App\Bitm\Users;
use App\Bitm\Utility\Utility;
use App\Bitm\Message\Message;

class  Auth{
    public $email="";
    public $user_name="";
    public $password="";
    public $passkey="";

    public  function prepare($data=Array()){

        if(array_key_exists('user_name',$data)){
            $this->user_name= $data['user_name'];
        }
        if(array_key_exists('password',$data)){
            $this->password= md5($data['password']);
        }
        if(array_key_exists('email',$data)){
            $this->email= $data['email'];
        }
        if(array_key_exists('passkey',$data)){
            $this->passkey= $data['passkey'];
        }

        return $this;

    }


    //////prepare connection
    public function __construct()
    {
        $this->conn= mysqli_connect("localhost","root","","pstock") or die("Database connection failed");
    }



    public function is_registered(){
        $query="SELECT * FROM `users` WHERE `user_name`='".$this->user_name."' AND `password`='".$this->password."'";
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_num_rows($result);
        if($row>0){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    public function is_exist(){
        $query="SELECT * FROM `users` WHERE `email`='".$this->email."'";
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_num_rows($result);
        if($row>0){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    public function viewuser(){
        $query="SELECT * FROM `users` WHERE `email`='".$this->email."'";
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;
    }

    public function is_checked(){
        $query="SELECT * FROM `reset_pass` WHERE `user_name`='".$this->user_name."' AND `status` IS NULL";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_num_rows($result);
        if($row>0){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    public function update_reset_pass(){
        $query="UPDATE `reset_pass` SET `mkey`='".$this->passkey."' WHERE `user_name`='".$this->user_name."' AND `status` IS NULL";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        //$row= mysqli_num_rows($result);
        if($result){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    public function verify_key(){
        $query="SELECT * FROM `reset_pass` WHERE `mkey`='".$this->passkey."' AND `status` IS NULL";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_num_rows($result);
        if($row>0){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    public function reset_pass(){
        $query1="SELECT * FROM `reset_pass` WHERE `mkey`='".$this->passkey."' AND `status` IS NULL";
        $result1= mysqli_query($this->conn,$query1);
        $row1=mysqli_fetch_assoc($result1);
//        var_dump($row1);
//        echo "</br>";
//        die();
        $nuser=$row1['user_name'];
        $query="UPDATE `users` SET `password`='".$this->password."' WHERE `user_name`='".$nuser."'";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        //$row= mysqli_num_rows($result);
        if($result){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    public function new_pass_req(){
        $query = "INSERT INTO `pstock`.`reset_pass` (`user_name`,`mkey`)
                        VALUES ('" . $this->user_name . "','" .$this->passkey . "')";
//        echo $query;
//        die();
        $result = mysqli_query($this->conn, $query);
        if($result){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    public function sendinfo(){
        $query="SELECT * FROM `reset_pass` WHERE `user_name`='".$this->user_name."' AND `status` IS NULL";
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;
    }

    public function is_requested(){
        $query="SELECT * FROM `reset_pass` WHERE `user_name`='".$this->email."'";
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_num_rows($result);
        if($row>0){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    public function logout()
    {
        if ((array_key_exists('ses_user', $_SESSION)) && (!empty($_SESSION['ses_user']))) {
            $_SESSION['ses_user'] = "";
            return TRUE;
        }

    }
    public function is_loggedin(){
        if((array_key_exists('ses_user',$_SESSION))&& (!empty($_SESSION['ses_user']))){
            return TRUE;
        }
        else{
            return FALSE;
        }

    }


}


