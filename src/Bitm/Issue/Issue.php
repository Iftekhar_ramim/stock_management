<?php
namespace App\Bitm\Issue;
use App\Bitm\Message\Message;
use App\Bitm\Utility\Utility;

Class Issue{
    public $id="";
    public $customer_id="";
    public $date="";
    public $description="";
    public $conn;

    public $cat_id;
    public $medi_id;
    public $unitid;
    public $qty;
    public $amount;

    public function __construct()
    {
        $this->conn= mysqli_connect("localhost","root","","pstock") or die("Database connection failed");
    }


    ////////prepare data


    public function prepare($data=Array()){
        if (array_key_exists("customer_id",$data)){
            $this->customer_id=$data['customer_id'];
        }
        if (array_key_exists("id",$data)){
            $this->id=$data['id'];
        }
        if (array_key_exists("date",$data)){
            $this->date=$data['date'];
        }
        if (array_key_exists("description",$data)){
            $this->description=$data['description'];
        }
        if (array_key_exists("itemid",$data)){
            $this->itemid=$data['itemid'];
        }


        if (array_key_exists("cat_id",$data)){
            $this->cat_id=$data['cat_id'];
        }

        if (array_key_exists("medi_id",$data)){
            $this->medi_id=$data['medi_id'];
        }

        if (array_key_exists("unitid",$data)){
            $this->unitid=$data['unitid'];
        }

        if (array_key_exists("qty",$data)){
            $this->qty=$data['qty'];
        }

        if (array_key_exists("amount",$data)){
            $this->amount=$data['amount'];
        }

        return $this;
    }

    //////prepare connection


    ////////prepare to store or insert data
    public function store()
    {

        $query = "INSERT INTO `pstock`.`issue_master` (`customer_id`,`date`,`description`)
                        VALUES ('" . $this->customer_id . "','" . $this->date . "','" . $this->description . "')";
        $result = mysqli_query($this->conn, $query);

        $master_id=$this->conn->insert_id;

        for($i=0;$i<count($this->cat_id);$i++) {

            $query1 = "INSERT INTO `pstock`.`issue_detail` (`master_id`,`master_date`,`cat_id`,`medi_id`,`unitid`,`qty`,`amount`)
                        VALUES ('" . $master_id . "','" . $this->date . "','" . $this->cat_id[$i] . "','" . $this->medi_id[$i] . "','" . $this->unitid[$i] . "','" . $this->qty[$i] . "','" . $this->amount[$i] . "')";
            $result1 = mysqli_query($this->conn, $query1);
        }
        //echo $query;
        //echo $query1;
        //die();

        if ($result&&$result1) {
            Message::message("<div class=\"alert alert-success\">
                                    <strong>Success!</strong> Data has been stored successfully.
                                    </div>");
            header('Location:create.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
                                    <strong>Error!</strong> Data has not been stored successfully.
                                    </div>");
            Utility::redirect('create.php');
        }
    }

    public function index(){
        $_allrcvmaster= array();
        $query="SELECT m.id,m.customer_id,s.name,m.date,m.description FROM issue_master m,customer s WHERE m.customer_id=s.id";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_allrcvmaster[]=$row;
        }
        return $_allrcvmaster;
    }

    public function trashlist(){
        $_alltrashed= array();
        $query="SELECT * FROM `issue_master` WHERE `deleted_at` IS NOT NULL";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_alltrashed[]=$row;
        }
        return $_alltrashed;
    }



    public function view(){
        $query="SELECT * FROM `issue_master` WHERE `id`=".$this->id;
        // echo $query;
        // die();
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;

    }

    public function dview(){
        $all=array();
        $query="SELECT * FROM `issue_detail` WHERE `master_id`=".$this->id;
        // echo $query;
        // die();
        $result= mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_assoc($result)){
            $all[]=$row;
        }
        return $all;

    }


    public function update(){
        $query="UPDATE `issue_master`
            SET `customer_id` = '".$this->customer_id."',`date` = '".$this->date."',`description` = '".$this->description."'
             WHERE `customer`.`id` =".$this->id;
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
            <strong>Updated!</strong> Data has been Updated successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-warning\">
            <strong>Error!</strong> Data has not been updated  successfully.
            </div>");
            Utility::redirect('index.php');

        }

    }

    public function delete(){
        $query="DELETE FROM `pstock`.`issue_master` WHERE `issue_master`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been deleted successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been deleted successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }

    public function trash(){
        $query="UPDATE `pstock`.`issue_master` SET `deleted_at` = '".time()."' WHERE `issue_master`.`id` = ".$this->id;
        //echo $query;
        //die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Trashed successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Trasded successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }

    public function restore(){
        $query="UPDATE `pstock`.`issue_master` SET `deleted_at` = NULL WHERE `issue_master`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Restore successfully.
            </div>");
            header('Location:trashed_list.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Restore successfully.
            </div>");
            Utility::redirect('trashed_list.php');
        }
    }

    public function trashmultiple(){
        $query="UPDATE `pstock`.`issue_master` SET `deleted_at` = '".time()."' WHERE `issue_master`.`id` IN(".$this->itemid.")";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Trashed successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Trashed successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }

    public function restoremultiple(){
        $query="UPDATE `pstock`.`issue_master` SET `deleted_at` = NULL WHERE `issue_master`.`id` IN(".$this->itemid.")";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Trashed successfully.
            </div>");
            header('Location:trashed_list.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Trashed successfully.
            </div>");
            Utility::redirect('trashed_list.php');
        }
    }

    public function deletemultiple(){
        $query="DELETE FROM `pstock`.`issue_master` WHERE `issue_master`.`id` IN(".$this->itemid.")";
//        echo $query;
//        die();
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
            <strong>Deleted!</strong> Data has been Deleted successfully.
            </div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
            <strong>Error!</strong> Data has not been Deleted successfully.
            </div>");
            Utility::redirect('index.php');
        }
    }

    public function dropdown(){
        $all_dropdown=array();
        $dropdown = "SELECT * FROM customer WHERE status='on'";
        $dropresult=mysqli_query($this->conn,$dropdown);
        while($row= mysqli_fetch_assoc($dropresult)){
            $all_dropdown[]=$row;
        }
        return $all_dropdown;
    }

    public function mdropdown(){
        $all_dropdown=array();
        $dropdown = "SELECT * FROM medicine WHERE status='on'";
        $dropresult=mysqli_query($this->conn,$dropdown);
        while($row= mysqli_fetch_assoc($dropresult)){
            $all_dropdown[]=$row;
        }
        return $all_dropdown;
    }
}//////////End Bracket